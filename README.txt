SaleMail module extends Drupal e-commerce module. Whenever a sale is made, SaleMail sends a mail to the administrator of the site, with details of the order.

SaleMail was developed by Jeff Veit of Tanasity Software + Services Ltd - http://www.tanasity.com/ 
Sponsorship from Aleph1 Ltd - http://www.aleph1.co.uk/

Notes:
======
SaleMail works with E-Commerce 3.3 and higher. It likely it works with earlier versions, but earlier versions are untested.

SaleMail has been tested with Drupal 5.2. It should work with earlier versions too. It should work equally well with Drupal 4.7.

The event sequence of EC 3.x makes it impossible for SaleMail to include the transaction ID to the administrator: the email is sent before the transaction ID has been established.
EC4 in contrast changes the event sequence, and allows you to include the transaction ID. 
The changes introduced in EC4 which allow this are easily ported back to EC3.

-----------------------------
For EC3.3 - to allow the inclusion of transaction ID:

***This operation should only be attempted by those who understand exactly what they are doing, the dangers involved, and who have the appropriate rights on the system.***

First you need to add the callbacks added in EC4...

Find your Drupal modules directory.
Open modules/ecommerce/cart/cart.module

Find the lines (dotted lines not included):
.........
   // If the transaction is done, tell them and send them home
    if ($txn->gross <= 0) {
      store_send_invoice_email($txn->txnid);
      drupal_set_message(t('Your order has been submitted.'));
      return '';
    }
.........
They should start at line 652.

After these lines, insert the following code (dotted lines excluded):
.........
    //This bit of code taken from EC4 to allow mails to be sent on order submission
    foreach ($txn->review_screens as $module) {
      $function = "{$module}_checkoutapi";
      function_exists($function) && $function($txn, 'post_process');
    }
    //End code insertion
.........

Save and close the file.



Now change salemail module to recognize the new callback....

open your salemail.module which will be in modules/salemail if you followed the standard installation

At or near line 13, find the line reading :
if ($op == 'review_submit') { 

Edit it to be:
if ($op == 'post_process') { 

Save and close the file. 



You are done. ID's will now show in emails to the admin.

-----------------------------